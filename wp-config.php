<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'devdesig_jpa' );

/** MySQL database username */
define( 'DB_USER', 'devdesig_jpa' );

/** MySQL database password */
define( 'DB_PASSWORD', '383&k(HmCI#%' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Vf$/!].]$3cUng>QOQ-UhiMThhEaJ!Ye.#NF5nuKK0re`ju5c[A4G{5P;J2XA?<4');
define('SECURE_AUTH_KEY',  '&ho0fK<R!r-&t9V |+|m%-3VCC$uaFA3(|B@-^$ayd9m9#{A:F}PjAO.hTo^/1`Z');
define('LOGGED_IN_KEY',    'z~O.:#1B{u$U9X?$HywGJ#+1S#QIL7Wn[<b>m}96*$j3cUvcR<e7=00hnXs+E;t+');
define('NONCE_KEY',        'J_5z3bk<f!96BB-H`|;M?P[e1EVY+k3HAQfW`s~*MT;U<z>G-#X.r]Yk<xX$ >w[');
define('AUTH_SALT',        '5Mb@.M$R];(+g#ocJ3uT:$mqp<E[#&Mz4jP7K-`=@{-mnreFRL<_dQfTr^F~0(:;');
define('SECURE_AUTH_SALT', 'w%BxuRR1[$K#1rRNLDT7!Y:6qL?QXU#E%ajevBo%T}m-?k[%>QO7)wd2o!ah2C-y');
define('LOGGED_IN_SALT',   '0-%*?.aW&2-Ka:! ljM<Xq=u%)$!jVKjXd.CO]C*<BHGrdxrWmdp3J7@H9TvE#18');
define('NONCE_SALT',       '.yfh5IOev2-aawA`!7>!(+QXg<Gd^Q-[ocGwIT5ns(ku}H-iInTz7j.:r;xgb+4`');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
